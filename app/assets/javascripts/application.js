//= require jquery
//= require d3
//= require nv.d3
//= require lib/angular.min
//= require lib/angular-ui-router.min
//= require lib/ui-bootstrap-tpls-0.11.0.min
//= require lib/modernizr.custom
//= require lib/jquery.dlmenu
//= require lib/angular-ui-tree
//= require lib/angularjs-nvd3-directives
//= require lib/lodash.compat.min
//= require lib/restangular
//= require_self
//= require_tree ./app

var jumbojet = angular.module('jumbojet', ['ui.router', 'ui.tree', 'ui.bootstrap', 'nvd3ChartDirectives', 'restangular']);
