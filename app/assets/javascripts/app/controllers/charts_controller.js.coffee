jumbojet.controller 'Charts', ($scope, $rootScope, $log, $state) ->
  # set rootScope.currentChartStyle by default to lineChart if route was access by url
  $rootScope.currentChartStyle ?= "barChart"

  $scope.backToTheBuilder = ->
    $state.go("builder")


  $scope.changeChartStyle = (chartStyle) ->
    $rootScope.currentChartStyle = chartStyle

  $scope.xFunction = ->
    (d) -> d.key

  $scope.yFunction = ->
    (d) -> d.y

  $scope.descriptionFunction = ->
    (d) -> d.key

  $scope.transactionsToShow = ->
    $rootScope.transactions.slice(0, 100)

  $scope.lineChartData =
  [{
    "key": "Series 1",
    values: $rootScope.lineChartData
  }]


  $scope.xAxisTickFormatFunction = ->
    return "2004"

  $scope.exampleDataPie = [
        { key: "One", y: 5 },
        { key: "Two", y: 2 },
        { key: "Three", y: 9 },
        { key: "Four", y: 7 },
        { key: "Five", y: 4 },
        { key: "Six", y: 3 },
        { key: "Seven", y: 9 }
     ]

  $scope.exampleDataBar = [
   {
      "key": "Series 1",
      "values": [ [ 1025409600000 , 12] , [ 1028088000000 , 16], [ 1035409600000 , 15] , [ 1048088000000 , 20]]
   }
  ]
