jumbojet.controller 'Builder', ($scope, $log, Restangular, $state, $rootScope) ->

  $scope.remove = (scope) ->
    scope.remove()
    return

  $scope.toggle = (scope) ->
    scope.toggle()
    return

  $scope.apply = (model) ->
    $.post("/api/transactions", data: $scope.builder).then( (data) ->
     $rootScope.transactions = data.transactions
     $rootScope.currentChartStyle = "barChart"
     $rootScope.lineChartData = data.line_chart
     console.log $rootScope.lineChartData

     $state.go("chart")
    , ->
     alert("Error")
    )

  $scope.builder = []

  $scope.column = [
    {
      id: 2
      title: 'Диапозон суммы'
      type: 'dataset'
      nodes: []
    }
    {
      id: 3
      title: 'Провайдер'
      system_title: "provider"
      type: 'checklist'
      value: $rootScope.providers
      nodes: []
    }
    {
      id: 3
      title: 'Город'
      system_title: "city_name"
      value: $rootScope.cities
      nodes: []
    }
    {
      id: 4
      title: 'Дата'
      value: $rootScope.cities
      nodes: []
    }
  ]
