jumbojet.config ($httpProvider, $stateProvider, $urlRouterProvider, $locationProvider) ->
  $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest'
  $locationProvider.html5Mode(true)

  $urlRouterProvider.otherwise("/")

  $stateProvider.state 'index',
    url: '/'
    views:
      content:
        templateUrl: '/templates/empty.html'
  .state 'builder',
    url: '/builder/:id'
    views:
      content:
        templateUrl: '/templates/builder.html'
  .state 'chart',
    url: '/chart/:id'
    views:
      content:
        templateUrl: '/templates/chart.html'

jumbojet.run ($rootScope) ->
  $rootScope.cities = JSON.parse($("meta[name=cities]").attr("content")).sort()
  $rootScope.providers = JSON.parse($("meta[name=providers]").attr("content"))
