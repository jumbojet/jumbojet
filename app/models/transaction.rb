class Transaction
  include Mongoid::Document

  field :date, type: DateTime
  field :phone_id, type: Integer
  field :range, type: String

  field :city_name, type: String
  field :provider, type: String

  def as_json(options)
    super(options.merge(except: "_id"))
  end
end
