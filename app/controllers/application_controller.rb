class ApplicationController < ActionController::Base
  before_filter :html_template

  def html_template
    if request.get? && request.format == Mime::HTML
      render 'home/index'
      false
    end
  end
end
