#encoding: utf-8
class TemplatesController < ApplicationController
  skip_before_filter :html_template

  def index
    render "home/index.html"
  end

  def show
    render "templates/#{params[:path]}", layout: nil
  end
end
