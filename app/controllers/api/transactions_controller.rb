class Api::TransactionsController < ApplicationController
  def index
    query = {}

    limit = 5000

    params[:data].try(:each) do |_, param|
      query[param["system_title"].downcase.to_sym] = param["selected"]
    end

    @transactions = Transaction.where(query).limit(limit)

    render json: {
      transactions: @transactions,
      line_chart: format_for_line_chart(@transactions)
    }

    return

    if query.blank?
      render json: { message: "No query supplied" }, status: 400 and return
    end

    render json: { transactions: Transaction.where(query).limit(limit) }
  end

  private

  def format_for_line_chart(transactions)
    # "values": [[ 1025409600000 , 0] , [ 1028088000000 , -6.3382185140371]]
    transactions.group_by { |tr| tr.date.beginning_of_day }.sort.map do |date, trs|
      [date.day, trs.size]
    end.sort_by(&:first)
  end
end
