Jumbojet::Application.routes.draw do
  root to: 'home#index'

  get 'templates/*path', to: 'templates#show'

  get '/*path', :to => 'templates#index'

  namespace :api do
    post "transactions" => "transactions#index"
  end
end
